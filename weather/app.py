import os

from flask import Flask,render_template, request,abort , send_file
import boto3
from boto3.session import  Session
import globalfunctions
import requests
app = Flask(__name__)

@app.route("/", methods=['get'])
def router_get():
    list_ = globalfunctions.get_coordinates("tel-aviv")
    dict_7_days = globalfunctions.get_dict_7_days(list_)
    dict_today = globalfunctions.get_dict_today(list_)
    return render_template("default.html",location="Tel-aviv"
                                      ,dict_7_days=dict_7_days
                                      ,dict_today=dict_today)

@app.route("/",methods=['post'])
def router_post():
    location = request.form.get("location")
    list_ =globalfunctions.get_coordinates(location)
    dict_7_days=globalfunctions.get_dict_7_days(list_)
    dict_today = globalfunctions.get_dict_today(list_)
    if list_ is None or dict_today is None or dict_7_days is None:
        abort(404, description=f"{location} location not found")
    return render_template('default.html',location=location
                                      ,dict_7_days=dict_7_days
                                      ,dict_today=dict_today)


@app.route("/sky", methods=['get'])
def get_pic():
    Access_KEY_ID = 'AKIAYPCXOVIZKO7QYD5Q'
    Secret_KEY = '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'
    session = Session(aws_access_key_id=Access_KEY_ID,
                      aws_secret_access_key=Secret_KEY)
    s3 = session.resource('s3')
    bucket = 'weatherpictures'

    my_bucket = s3.Bucket(bucket)
    for s3_files in my_bucket.objects.all():
        print(s3_files.key)
    s3 = boto3.client('s3')
    try:
        my_bucket.download_file('index', './sky.jpeg')
        return send_file('./sky.jpeg', as_attachment=True)
    finally:
        os.remove('./sky.jpeg')

@app.route("/dynamo", methods=['get'])
def create_table():
    list_ = globalfunctions.get_coordinates("tel-aviv")
    dict_today = globalfunctions.get_dict_today(list_)
    Access_KEY_ID = 'AKIAYPCXOVIZKO7QYD5Q'
    Secret_KEY = '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'
    session = Session(aws_access_key_id=Access_KEY_ID,
                      aws_secret_access_key=Secret_KEY)

    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table('Daily')
    if table is not None:
        table = dynamodb.create_table(
            TableName='Daily',
            KeySchema=[
                {
                    'AttributeName': 'Date',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'Date',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        table.wait_until_exists()

    table= dynamodb.Table('Daily')
    table.put_item(
        Item={
            'Date' : dict_today["today"],
            'Temperature' : dict_today["temp"],
            'Location' : dict_today["location"],
            'Description' : dict_today["description"]
        }
    )
    return ("success")


if __name__ == '__main__':
    app.run(debug=True)