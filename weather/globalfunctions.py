import requests
from datetime import timedelta
from datetime import date


def day_of_week(index):
    today = date.today() + timedelta(days=index)
    day = today.day
    years= today.year
    month= today.month
    int_day=date(years,month,day).weekday()
    weekly= ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    return weekly[int_day]

def get_coordinates(location):
    list_=[]
    APIkey = "77d7d62a02bf7662f5406c63c27ee0e2"
    r = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={location}&limit=5&appid={APIkey}")
    if r.status_code == 200 and len(r.json()) != 0:
        lat = float(dict(r.json()[0]).get("lat"))
        lon = float(dict(r.json()[0]).get("lon"))
        country=dict(r.json()[0]).get("country")
        list_.append(lat)
        list_.append(lon)
        list_.append(country)
        return list_
    return None

def get_dict_7_days(list_):
    if list_ is not None:
        result = dict()
        APIkey = "77d7d62a02bf7662f5406c63c27ee0e2"
        part = "current,minutely,hourly,alerts"
        y=requests.get(f"https://api.openweathermap.org/data/2.5/onecall?lat={list_[0]}&lon={list_[1]}&exclude={part}&appid={APIkey}&units=metric")
        if y.status_code == 200 and len(y.json()) != 0:
            y=dict(y.json())
            for i in range(7):
                current_date=y["daily"][i]["temp"]
                day=int(current_date["day"])
                night=int(current_date["night"])
                icon="http://openweathermap.org/img/wn/"+y["daily"][i]["weather"][0]["icon"]+"@2x.png"
                result[i] = {"date": day_of_week(i),"icon":icon,"day":day,"night":night}
            return result
    return None

def get_dict_today(list_):
    if list_ is not None:
        result=dict()
        APIkey = "77d7d62a02bf7662f5406c63c27ee0e2"
        part = "current,minutely,hourly,alerts"
        y = requests.get(f"https://api.openweathermap.org/data/2.5/onecall?lat={list_[0]}&lon={list_[1]}&exclude={part}&appid={APIkey}&units=metric")
        if y.status_code == 200 and len(y.json()) != 0:
            y = dict(y.json())
            temp=int(y["daily"][0]["temp"]["day"])
            humidity=y["daily"][0]["humidity"]
            clouds=y["daily"][0]["clouds"]
            wind_speed=int(y["daily"][0]["wind_speed"])
            today = date.today().strftime("%B %d, %Y")
            location=list_[2]
            description=y["daily"][0]["weather"][0]["description"]
            icon="http://openweathermap.org/img/wn/"+y["daily"][0]["weather"][0]["icon"]+"@2x.png"
            result = {
                      "description":description,
                      "temp":temp,
                      "humidity":humidity,
                      "clouds":clouds,
                      "wind_speed":wind_speed,
                      "today":today,
                      "location":location,
                      "icon":icon
                      }
            return result
    return None

list_ = get_coordinates("london")
print(get_dict_7_days(list_))
print(get_dict_today(list_))


