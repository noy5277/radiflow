FROM ubuntu:20.04

RUN apt-get update -y
RUN apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools -y

RUN mkdir ~/myproject

WORKDIR ~/myproject
COPY requirements.txt requirements.txt
COPY weather .
RUN pip3 install -r requirements.txt
RUN chmod +x weather-play.sh

CMD ./weather-play.sh
